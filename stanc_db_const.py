import subprocess

File_Path = "/jboss/tomcat/jws5/instances/jws-tomcat-app/bin/setenv.sh"

DB_Name_String_full = subprocess.check_output("cat " + File_Path + " | grep DATABASE_URL", shell=True)
DB_Name_String_Split = str(DB_Name_String_full).split('//')
DB_Name_String_Final = str(DB_Name_String_Split[1]).split(':')

db_hostname = DB_Name_String_Final[0]


DB_Passwd_String_full = subprocess.check_output("cat " + File_Path + " | grep DATABASE_PASSWORD", shell=True)
DB_Passwd_String_Split = str(DB_Passwd_String_full).split('=')
DB_Passwd_String_Final = str(DB_Passwd_String_Split[1]).split('\'')
db_password = DB_Passwd_String_Final[1]

DB_NAME_String_full = subprocess.check_output("cat " + File_Path + " | grep DATABASE_USER", shell=True)
DB_NAME_String_Split = str(DB_NAME_String_full).split('=')
db_username = DB_NAME_String_Split[1].split('\\')[0]

db_port = 5401
db_name = 'sgkulizadb'

POSTGRES_SOURCE_TABLE = dict({
    "dbname": db_name,
    "user": db_username,
    "password": db_password,
    "host": db_hostname,
    "port": db_port,
    "schema": "lendin"
})

IB_HOST_String_full = subprocess.check_output("cat " + File_Path + " | grep IB_HOST", shell=True)
IB_HOST_String_Split = str(IB_HOST_String_full).split('=')
IB_HOST_URL = IB_HOST_String_Split[1].split('\\')[0]

ENVIRONMENT_String_full = subprocess.check_output("cat " + File_Path + " | grep ENVIRONMENT", shell=True)
ENVIRONMENT_String_Split = str(ENVIRONMENT_String_full).split('=')
ENVIRONMENT = ENVIRONMENT_String_Split[1].split('\\')[0]

EDMP_HOST_String_full = subprocess.check_output("cat " + File_Path + " | grep EDMP_FTP_HOST", shell=True)
EDMP_HOST_String_Split = str(EDMP_HOST_String_full).split('=')

EDMP_FTP_HOST = EDMP_HOST_String_Split[1].split('\\')[0]

EDMP_HOST_UNAME_String_full = subprocess.check_output("cat " + File_Path + " | grep EDMP_FTP_USERNAME", shell=True)
EDMP_HOST_UNAME_String_Split = str(EDMP_HOST_UNAME_String_full).split('=')

EDMP_FTP_USERNAME = EDMP_HOST_UNAME_String_Split[1].split('\\')[0]

PDW_HOST_String_full = subprocess.check_output("cat " + File_Path + " | grep PDW_FTP_HOST", shell=True)
PDW_HOST_String_Split = str(PDW_HOST_String_full).split('=')

PDW_FTP_HOST = PDW_HOST_String_Split[1].split('\\')[0]

PDW_HOST_UNAME_String_full = subprocess.check_output("cat " + File_Path + " | grep PDW_FTP_USERNAME", shell=True)
PDW_HOST_UNAME_String_Split = str(PDW_HOST_UNAME_String_full).split('=')

PDW_FTP_USERNAME = PDW_HOST_UNAME_String_Split[1].split('\\')[0]

EDMP_FTP_PORT = 22
EDMP_FTP_PRIVATE_KEY_FILE_PATH = "/scb-etl/edmp/hkedm2ft"
EDMP_HOST_PATH_String_full = subprocess.check_output("cat " + File_Path + " | grep EDMP_FTP_FILEPATH", shell=True)
EDMP_HOST_PATH_String_Split = str(EDMP_HOST_PATH_String_full).split('=')
EDMP_FTP_REMOTE_FILE_PATH = EDMP_HOST_PATH_String_Split[1].split('\\')[0]

PDW_FTP_PORT = 22
PDW_FTP_PRIVATE_KEY_FILE_PATH = "/scb-etl/pdw/51205lendin-pdw-sftp"
PDW_HOST_PATH_String_full = subprocess.check_output("cat " + File_Path + " | grep PDW_FTP_FILEPATH", shell=True)
PDW_HOST_PATH_String_Split = str(PDW_HOST_PATH_String_full).split('=')
PDW_FTP_REMOTE_FILE_PATH = PDW_HOST_PATH_String_Split[1].split('\\')[0]